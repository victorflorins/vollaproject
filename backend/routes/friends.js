const router = require('express').Router();
const Friend = require('../models/Friend');
const Group = require('../models/Group');
const User = require('../models/User');

router.get('/', (req, res) => {
    Friend.findAll({
        attributes: ['id', 'groupId'],
        include: [
            { model: User, attributes: ['id', 'firstName', 'lastName'], as: 'user' }
        ]
    })
        .then(friends => res.send(friends))
        .catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
})

router.post('/delete', (req, res) => {
    const { userId, groupId} = req.body
    if (!userId||!groupId) {
        res.status(404).send({message:"Invalid payload"});
    } else {
        Friend.destroy({where : {userId,groupId}}).then(result =>{
            if(result){
                res.status(200).send({
                    message : `Deleted ${result} rows`
                })
            }else{
                res.status(200).send({
                    message : 'No records found matching the given id'
                })
            }
        }).catch(err => {
            console.log(err)
            res.status(500).send({
                message : 'Something went wrong'
            })
        })
    }
})

router.post('/', (req, res) => {
    const { userEmail, groupId } = req.body;
    if (!userEmail || !groupId) {
        res.status(400).send({ message: 'Wrong body format!' });
    } else {
        User.findOne({where: {email:userEmail}}).then((user)=>{
            if(!user){
                res.status(404).send({message:"Email does not exist"});
            }
            else{
                Friend.findOne({ where: { userId:user.id, groupId } })
                .then(friend => {
                    if (friend !== null) {
                        res.status(400).send({ message: 'This friend already exists in this group!' });
                    } else {
                        Group.findOne({ where: { id: groupId }, raw: true })
                            .then(group => {
                                if (!group) {
                                    res.status(404).send({ message: 'This group doesn\'t exist' });
                                } else {
                                    if (group.ownerId === user.id) {
                                        res.status(400).send({ message: 'A user can\'t add himself as a friend!' })
                                    } else {
                                        Friend.create({ userId:user.id, groupId })
                                            .then(() => res.status(200).send({ message: 'Success!' }))
                                            .catch(error => {
                                                console.log(error);
                                                res.status(500).send({ message: 'Something went wrong! Try again later!' });
                                            })
                                    }
                                }
                            })
                    }
                }).catch(error => {
                    console.log(error);
                    res.status(500).send({ message: 'Something went wrong! Try again later!' });
                })
            }
        }).catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
    }
})

module.exports = router;