const bcrypt = require('bcrypt');
const router = require('express').Router();
const User = require('../models/User');
const Group = require('../models/Group');
const Product = require('../models/Product');
const Category = require('../models/Category');
const Friend = require('../models/Friend');
const Invite = require ('../models/Invite');
var Sequelize = require('sequelize');
const Op = Sequelize.Op;


router.get('/', (req, res) => {
    User.findAll({ raw: true, attributes: ['id', 'firstName', 'lastName'] })
        .then(users => res.send(users))
        .catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
})

router.post('/login', (req, res) => {
    const { email, password } = req.body;
    if (!email || !password) {
        res.status(400).send({ message: 'Wrong body format!' });
    } else {
        User.findOne({ where: { email } }).then(user => {
            if (user) {
                bcrypt.compare(password, user.passwordHash, (error, result) => {
                    if (error) {
                        console.log(error);
                        res.status(500).send({ message: 'Something went wrong! Try again later!' });
                    } else {
                        if (result) {
                            res.cookie('userData', { id: user.id, email: user.email, firstName: user.firstName, lastName: user.lastName });
                            res.status(200).send({ message: 'Success!', userId:user.id });
                        } else {
                            res.status(400).send({ message: 'Email or password incorrect!' });
                        }
                    }
                })
            } else {
                res.status(400).send({ message: 'Email or password incorrect!' });
            }
        }).catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
    }
})

router.post('/register', (req, res) => {
    const { lastName, firstName, email, password } = req.body;
    if (!lastName || !firstName || !email || !password) {
        res.status(400).send({ message: 'Wrong body format!' });
    } else {
        User.findOne({ where: { email } }).then(user => {
            if (!user) {
                bcrypt.hash(password, 10, (error, passwordHash) => {
                    if (error) {
                        console.log(error);
                        res.status(500).send({ message: 'Something went wrong! Try again later!' });
                    } else {
                        User.create({ firstName, lastName, email, passwordHash }).then(() => res.send({ message: 'Success!' }))
                            .catch(error => {
                                console.log(error);
                                res.status(500).send({ message: 'Something went wrong! Try again later!' });
                            })
                    }
                })
            } else {
                res.status(400).send({ message: 'Email already exists!' });
            }
        }).catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
    }
})

router.get('/:user_id/groups', (req, res) => {
    const userId = Number(req.params.user_id);
    if ( isNaN(userId)) {
        res.status(404).send();
    } else {
        Group.findAll({
            attributes: ['id', 'label', 'ownerId'],
            include: [{
                model: Friend,
                as: 'friends',
                where: { userId },
                include: [{
                    model: User,
                    as: 'user',
                    attributes: ['id', 'firstName', 'lastName']
                }]
            },
            {
                model: User,
                as: 'owner',
                attributes: ['id', 'firstName', 'lastName']
            }]
        }).then(groups => {
            Group.findAll({
                where:{
                    ownerId:userId
                },
                include: [{
                    model: Friend,
                    as: 'friends',
                    include: [{
                        model: User,
                        as: 'user',
                        attributes: ['id', 'firstName', 'lastName']
                    }]
                }]
            }).then(myGroups=>{
                let all=myGroups.concat(groups)
                res.send(all);
            }).catch(error => {
                console.log(error);
                res.status(500).send({ message: 'Something went wrong! Try again later!' });
            })
        }).catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
    }
})

router.get('/:user_id/products', (req, res) => {
    const userId = Number(req.params.user_id);
    if (isNaN(userId)) {
        res.status(404).send();
    } else {
        Product.findAll({
            where: { userId },
            attributes: ['id', 'name', 'available', 'expireDate'],
            include: [
                { model: Category, as: 'category' }
            ]
        }).then(fridge => {
            res.send(fridge);
        }).catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
    }
})

router.get('/:user_id/categories', (req, res) => {
    const userId = Number(req.params.user_id);
    if (isNaN(userId)) {
        res.status(404).send();
    } else {
        Category.findAll({
            where: { userId },
            attributes: ['id', 'name'],
        }).then(categories => {
            res.send(categories);
        }).catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
    }
})

router.get('/:user_id/info', (req, res) => {
    const id = Number(req.params.user_id);
    if (isNaN(id)) {
        res.status(404).send();
    } else {
        User.findOne({
            where: { id },
            attributes: ['id', 'firstName', 'lastName'],
            include: [
                {
                    model: Group, as: 'groups', attributes: ['id', 'label'], include: [
                        {
                            model: Friend, as: 'friends', attributes: ['id'], include: [
                                { model: User, attributes: ['id', 'firstName', 'lastName'], as: 'user' }
                            ]
                        }
                    ]
                },
                {
                    model: Product, as: 'products', attributes: ['id', 'name', 'available', 'expireDate'], include: [
                        { model: Category, as: 'category' }
                    ]
                }
            ]
        }).then(groups => {
            res.send(groups);
        }).catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
    }
})


router.get('/:user_id/invites_sent', (req, res) => {
    const ownerId = Number(req.params.user_id);
    if (isNaN(ownerId)) {
        res.status(404).send();
    } else {
        Invite.findAll({
            where: { ownerId }
        }).then(invites => {
            res.send(invites);
        }).catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
    }
})

router.get('/:user_id/invites_received', (req, res) => {
    const invitedId = Number(req.params.user_id);
    if (isNaN(invitedId)) {
        res.status(404).send();
    } else {
        Invite.findAll({
            where: { invitedId }
        }).then(invites => {
            res.send(invites);
        }).catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
    }
})

router.post('/:user_id/invite', (req, res) => {
    const ownerId = Number(req.params.user_id);
    const invitedId=req.body.invitedId
    if (isNaN(ownerId) || !invitedId) {
        res.status(404).send();
    } else {
        Invite.create({ ownerId, invitedId }).then(() => res.send({ message: 'Success!' }))
            .catch(error => {
                console.log(error);
                res.status(500).send({ message: 'Something went wrong! Try again later!' });
            })
    }
})

router.post('/:user_id/uninvite/:uninvited_id', (req, res) => {
    const ownerId = Number(req.params.user_id);
    const invitedId = Number(req.params.uninvited_id);
    if (isNaN(ownerId) || isNaN(invitedId)) {
        res.status(404).send();
    } else {
        Invite.destroy({where : {ownerId,invitedId}}).then(result =>{
            if(result){
                res.status(200).send({
                    message : `Deleted ${result} rows`
                })
            }else{
                res.status(200).send({
                    message : 'No records found matching the given id'
                })
            }
        }).catch(err => {
            console.log(err)
            res.status(500).send({
                message : 'Something went wrong'
            })
        })
    }
})


router.get('/:user_id/claims', (req, res) => {
    const userId = Number(req.params.user_id);
    if (isNaN(userId)) {
        res.status(404).send();
    } else {
        Product.findAll({
            where: { claimedBy:userId }
        }).then(claims => {
            res.send(claims);
        }).catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
    }
})


module.exports = router;