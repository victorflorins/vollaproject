const router = require('express').Router();
const User = require('../models/User');
const Group = require('../models/Group');
const Friend = require('../models/Friend');

router.get('/', (req, res) => {
    Group.findAll({ raw: true })
        .then(groups => res.send(groups))
        .catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
})

router.delete('/:id', (req, res) => {
    const id = req.params.id
    if (!id) {
        res.status(404).send();
    } else {
       Group.destroy({where : {id}}).then(result => {
           if(result){
               res.status(200).send({
                   message : `Deleted ${result} rows`
               })
           }else{
               res.status(200).send({
                   message : 'No records found matching the given id'
               })
           }
       }).catch(err =>{
           console.log(err)
           res.status(500).send({
               message : 'Something went wrong'
           })
       })
    }
})

router.put('/:id', (req, res) => {
    const id = Number(req.params.id);
    const { label } = req.body;
    if (!id) {
        res.status(404).send();
    } else {
        Group.update({ label }, {
            where: { id }
        }).then(() => res.send({ message: 'Success!' }))
            .catch(error => {
                console.log(error);
                res.status(500).send({ message: 'Something went wrong! Try again later!' });
            })
    }
})

router.post('/', (req, res) => {
    const { ownerId, label } = req.body;
    if (!ownerId || !label) {
        res.status(400).send({ message: 'Wrong body format!' });
    } else {
        Group.create({ ownerId, label })
            .then(() => res.send({ message: 'Success!' }))
            .catch(error => {
                console.log(error);
                res.status(500).send({ message: 'Something went wrong! Try again later!' });
            })
    }
})

router.get('/:id/friends', (req, res) => {
    const id = Number(req.params.id);
    if (!id) {
        res.status(404).send();
    } else {
        Group.findOne({
            where: { id },
            attributes: ['id', 'label'],
            include: [
                {
                    model: Friend, as: 'friends', attributes: ['id'], include: [
                        { model: User, attributes: ['id', 'firstName', 'lastName'], as: 'user' }
                    ]
                }
            ]
        }).then(group => res.send(group))
            .catch(error => {
                console.log(error);
                res.status(500).send({ message: 'Something went wrong! Try again later!' });
            })
    }
})

module.exports = router;