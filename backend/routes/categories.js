const router = require('express').Router();
const Category = require('../models/Category');
const Products = require('../models/Product');

router.get('/', (req, res) => {
    Category.findAll({
        raw: true
    })
        .then(categories => res.send(categories))
        .catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
})

router.post('/', (req, res) => {
    const { name, userId } = req.body;
    Category.create({ name, userId })
        .then(() => res.send({ message: 'Success' }))
        .catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
})

router.delete('/delete', (req, res) => {
    const id = req.body
    if (!id) {
        res.status(404).send();
    } else {
        Category.destroy({where : id}).then(result =>{
            if(result){
                res.status(200).send({
                    message : `Deleted ${result} rows`
                })
            }else{
                res.status(200).send({
                    message : `No records found matching the id given`
                })
            }
        }).catch(err =>{
            console.log(err)
            res.status(500).send({
                message : 'Something went wrong'
            })
        })
    }
})

router.put('/:id', (req, res) => {
    const id = Number(req.params.id);
    if (isNaN(id)) {
        res.status(404).send();
    } else {
        const { name } = req.body;
        Category.update({ name }, { where: { id } })
            .then(() => res.send({ message: 'Success' }))
            .catch(error => {
                console.log(error);
                res.status(500).send({ message: 'Something went wrong! Try again later!' });
            })
    }
})

router.get('/:category_id/products', (req, res) => {
    const categoryId = Number(req.params.category_id);
    if (isNaN(categoryId)) {
        res.status(404).send();
    } else {
        Products.findAll({
            where: { categoryId },
            attributes: ['id', 'name', 'available', 'expireDate','claimedBy'],
        }).then(products => {
            res.send(products);
        }).catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
    }
})

module.exports = router;