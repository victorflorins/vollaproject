const router = require('express').Router();
const Product = require('../models/Product')

router.get('/', (req, res) => {
    Product.findAll({
        raw: true
    }).then(products => res.send(products))
        .catch(error => {
            console.log(error);
            res.status(500).send({ message: 'Something went wrong! Try again later!' });
        })
})

router.post('/', (req, res) => {
    const { name, expireDate, available, userId, categoryId } = req.body;
    if (!name || !expireDate || typeof available === 'undefined' || !userId || !categoryId) {
        res.status(400).send({ message: 'Wrong body format' });
    } else {
         Product.create({ name, expireDate, available, userId, categoryId })
                .then(() => res.send({ message: 'Success' }))
                .catch(error => {
                    console.log(error);
                    res.status(500).send({ message: 'Something went wrong! Try again later!' });
                })
    }
})

router.delete('/:id', (req, res) => {
    const id = Number(req.params.id);
    if (isNaN(id)) {
        res.status(404).send();
    } else {
        Product.destroy({where : {id}}).then(result => {
            if(result){
                res.status(200).send({
                    message : `Deleted ${result} rows`
                })
            }else{
                res.status(200).send({
                    message : `No records found matching the given id`
                })
            }
        }).catch(err => {
            console.log(err)
            res.status(500).send({
                message : 'Something went wrong'
            })
        })
    }
})

router.put('/:id', (req, res) => {
    const id = Number(req.params.id);
    if (isNaN(id)) {
        res.status(404).send();
    } else {
        const { name, expireDate, available, userId, categoryId, claimedBy } = req.body;
        Product.update({ name, expireDate, available, userId, categoryId, claimedBy }, { where: { id } })
            .then(() => res.send({ message: 'Success' }))
            .catch(error => {
                console.log(error);
                res.status(500).send({ message: 'Something went wrong! Try again later!' });
            })
    }
})

router.post('/:id/claim',(req, res) => {
    const id = Number(req.params.id);
    const {userId}=req.body;
    if (isNaN(id)||!userId) {
        res.status(404).send();
    } else {
        Product.update({ claimedBy:userId }, { where: { id } })
            .then(() => res.send({ message: 'Success' }))
            .catch(error => {
                console.log(error);
                res.status(500).send({ message: 'Something went wrong! Try again later!' });
            })
    }
})

router.post('/:id/unclaim',(req, res) => {
    const id = Number(req.params.id);
    const {userId}=req.body;
    if (isNaN(id)||!userId) {
        res.status(404).send();
    } else {
        Product.update({ claimedBy:-1 }, { where: { id } })
            .then(() => res.send({ message: 'Success' }))
            .catch(error => {
                console.log(error);
                res.status(500).send({ message: 'Something went wrong! Try again later!' });
            })
    }
})

module.exports = router;