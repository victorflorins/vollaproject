const db = require('../config/db');
const Sequelize = require('sequelize');
module.exports = db.define('user', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    firstName: Sequelize.STRING,
    lastName: Sequelize.STRING,
    email: Sequelize.STRING,
    passwordHash: Sequelize.STRING
}, {
        timestamps: false
    })
