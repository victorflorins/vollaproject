const Sequelize = require('sequelize');
const db = require('../config/db');
const User = require('./User');
const Category = require('./Category');

module.exports = db.define('product', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: Sequelize.STRING,
    expireDate: Sequelize.DATE,
    available: Sequelize.BOOLEAN,
    userId: {
        type: Sequelize.INTEGER,
        references: {
            model: User,
            key: 'id'
        }
    },
    categoryId: {
        type: Sequelize.INTEGER,
        references: {
            model: Category,
            key: 'id'
        }
    },
    claimedBy:{
        type: Sequelize.INTEGER,
        defaultValue: -1 
    }
}, {
        timestamps: false
    })