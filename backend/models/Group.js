const Sequelize = require('sequelize');
const db = require('../config/db');
const User = require('./User');

module.exports = db.define('group', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    label: Sequelize.STRING,
    ownerId: {
        type: Sequelize.INTEGER,
        references: {
            model: User,
            key: 'id'
        }
    }
}, {
        timestamps: false
    })