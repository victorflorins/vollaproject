const User = require('./User');
const Group = require('./Group');
const Friend = require('./Friend');
const Category = require('./Category');
const Product = require('./Product');
const Invite=require('./Invite');

module.exports = function () {
    User.hasMany(Group, {
        as: 'groups',
        foreignKey: 'ownerId'
    })
    
    Group.belongsTo(User, {
        as: 'owner',
        foreignKey: 'ownerId'
    })

    Group.hasMany(Friend, {
        as: 'friends',
        foreignKey: 'groupId'
    })

    Friend.belongsTo(User, {
        as: 'user',
        foreignKey: 'userId'
    })
    
    Category.belongsTo(User, {
        as: 'user',
        foreignKey: 'userId'
    })

    Category.hasMany(Product, {
        as: 'products',
        foreignKey: 'categoryId',
    })

    Product.belongsTo(Category, {
        as: 'category',
        foreignKey: 'categoryId'
    })

    User.hasMany(Product, {
        as: 'products',
        foreignKey: 'userId',
    })
    
    User.hasMany(Category, {
        as: 'categories',
        foreignKey: 'userId',
    })
    
    
    User.hasMany(Invite, {
        as: 'invites',
        foreignKey: 'ownerId'
    })
    
    User.hasMany(Invite, {
        as: 'invitesReceived',
        foreignKey: 'invitedId'
    })

}
