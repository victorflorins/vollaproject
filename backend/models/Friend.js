const Sequelize = require('sequelize');
const db = require('../config/db');
const User = require('./User');
const Group = require('./Group');

module.exports = db.define('friend', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    userId: {
        type: Sequelize.INTEGER,
        references: {
            model: User,
            key: 'id'
        }
    },
    groupId: {
        type: Sequelize.INTEGER,
        references: {
            model: Group,
            key: 'id'
        }
    },
}, {
        timestamps: false
    })