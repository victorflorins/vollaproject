const Sequelize = require('sequelize');
const { dbHost, dbUser, dbPass, dbName } = require('./config');
module.exports = new Sequelize(
    dbName,
    dbUser,
    dbPass, {
        host: dbHost,
        dialect: 'mysql',
    }
)