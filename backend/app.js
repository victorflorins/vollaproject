const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors=require("cors");

const users = require('./routes/users');
const groups = require('./routes/groups');
const friends = require('./routes/friends');
const categories = require('./routes/categories');
const products = require('./routes/products');


app.use(cors());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/users', users);
app.use('/groups', groups);
app.use('/friends', friends);
app.use('/categories', categories);
app.use('/products', products);

module.exports = app; 