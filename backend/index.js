const app = require('./app');
const db = require('./config/db');
require('./models/associations')();

//db.sync({force:true});

const { port } = require('./config/config');
const server = app.listen(port, () => {
    console.log(`server started on port ${port}...`);
})


