import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Login from './Login/Login';
import Register from './Register/Register';
import Home from './Home/Home';
import './App.css'

class App extends React.Component {
  
  constructor(props){
    super(props)
    this.state={
      logat:false,
      userId:-1
    }
    
    this.login=(userId)=>{
      this.setState({logat:true, userId})
    }
    this.logout=()=>{
      this.setState({logat:false})
    }
  }

  
  render() {
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route exact path='/login'><Login logat={this.state.logat} login={this.login}/></Route>
            <Route exact path='/register'><Register/></Route>
            <Route path='/'><Home logat={this.state.logat} logout={this.logout} userId={this.state.userId}/></Route>
          </Switch>
        </Router>
        
      </div>
    );
  }
}

export default App;
