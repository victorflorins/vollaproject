import React from 'react';
import axios from 'axios';
import Category from './Category';
import SERVER from '../../config/config';
import { withRouter } from "react-router-dom";
import './FriendsList.css';


class FriendsList extends React.Component {
    
    constructor(props){
        super(props);
        this.state={
            categories: [],
            error:""
        }
        
        this.getCategories=async () =>{
            try{
                let categories=await axios.get(`${SERVER}/users/${this.props.match.params.userId}/categories`);
                this._isMounted && this.setState({
                    categories:categories.data
                });
            }
            catch(e){
                this._isMounted && this.setState({error:"Something went wrong"})
            }
        }
    
        this._isMounted = false;
    }
    
    componentDidMount = ()=>{
        this._isMounted = true;
        this._isMounted && this.getCategories()
    }
    
    componentWillUnmount() {
       this._isMounted = false;
    }
    
    render() {
        return (
            <div className="ProductList">
                <div className="header">
                    <h1>{`${this.props.location.state.user.lastName} ${this.props.location.state.user.firstName}'s list`}</h1>
                </div>
                {this.state.error && <div>{this.state.error}</div>}
                
                {this.state.categories.map((category,index)=><Category key={index} userId={this.props.match.params.userId} category={category} currentUser={this.props.currentUser}/>)}
            </div>
        );
    }
}

export default withRouter(FriendsList);