import React from 'react';
import {withRouter } from "react-router-dom";
import {Menubar} from 'primereact/menubar';
import './Menu.css'

class Menu extends React.Component{
    
    constructor() {
        super();
        this.state = {
            items:[
                {label:'Home',icon:'pi pi-home',command:()=>{this.props.history.push('/')}},
                {label:'My products',icon:'pi pi-list',command:()=>{this.props.history.push('/myproducts')}},
                {label:'Friends',icon:'pi pi-users',command:()=>{this.props.history.push('/friends')}},
                {label:'Logout',icon:'pi pi-power-off',command:(e)=>{this.props.logout()}}
            ]
        };
    }

    render() {
        return (
            <div className="Menubar" style={{marginBottom:"30px"}}>
                <Menubar model={this.state.items}>
                </Menubar>
            </div>
        );
    }
}

export default withRouter(Menu)