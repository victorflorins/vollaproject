import React from 'react';
import {withRouter} from 'react-router-dom';
import './Login.css';

import axios from 'axios';
import SERVER from '../../config/config'


class Login extends React.Component {
    
    constructor(props){
        super(props)
        
        this.state={
            email:"",
            password:"",
            error:""
        }
    }
    
    componentDidMount(){
        if(this.props.logat){
            this.props.history.push('/')
        }
    }
    
    render(){
        let eroare;
        if(this.state.error){
            eroare=<div className="eroare">{this.state.error}</div>;
        }
        
        return(
            <div className="loginBox">
                <input type="text" value={this.state.email} placeholder="Email" onChange={(e)=>{this.setState({email:e.target.value})}} />
                <input type="password" value={this.state.password} placeholder="Password" onChange={(e)=>{this.setState({password:e.target.value})}} />
                <div>
                    <button onClick={async (e)=>{
                        this.setState({error:""})
                        if(this.state.email&&this.state.password){
                            try{
                                let response=await axios.post(`${SERVER}/users/login`,{
                                    email: this.state.email,
                                    password: this.state.password
                                });
                                if(response.status===200){
                                    this.props.login(response.data.userId);
                                    this.props.history.push('/');
                                }
                                else{
                                    this.setState({error:response.data.message})
                                }
                            }
                            catch(e){
                                this.setState({error:e.response.data.message})
                            }
                        }
                        else{
                            this.setState({error:"All fields are mandatory!"})
                        }
                    }}>Login</button>
                    <button onClick={(e)=>{this.props.history.push('/register')}}>Register</button>
                </div>
                
                {eroare}
            </div>
        );
        
    }
    
}

export default withRouter(Login);