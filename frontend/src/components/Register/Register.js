import React from 'react';
import {withRouter} from 'react-router-dom';
import './Register.css';

import axios from 'axios';
import SERVER from '../../config/config'


class Register extends React.Component {
    
    constructor(props){
        super(props)
        
        this.state={
            firstName:"",
            lastName:"",
            email:"",
            password:"",
            error:""
        }
        
    }
    
    render(){
        
        let eroare;
        if(this.state.error){
            eroare=<div className="eroare">{this.state.error}</div>;
        }
        
        return(
            <div className="registerBox">
                <input type="text" value={this.state.firstName} placeholder="First name" onChange={(e)=>{this.setState({firstName:e.target.value})}} />
                <input type="text" value={this.state.lastName} placeholder="Last name" onChange={(e)=>{this.setState({lastName:e.target.value})}} />
                <input type="text" value={this.state.email} placeholder="Email" onChange={(e)=>{this.setState({email:e.target.value})}} />
                <input type="password" value={this.state.password} placeholder="Password" onChange={(e)=>{this.setState({password:e.target.value})}} />
                <div>
                    <button onClick={async (e)=>{
                        this.setState({error:""})
                        if(this.state.email&&this.state.password&&this.state.firstName&&this.state.lastName){
                            try{
                                let response=await axios.post(`${SERVER}/users/register`,{
                                    firstName: this.state.firstName,
                                    lastName: this.state.lastName,
                                    email: this.state.email,
                                    password: this.state.password
                                });
                                if(response.status===200){
                                    this.props.history.push('/login');
                                }
                                else{
                                    this.setState({error:response.data.message})
                                }
                            }
                            catch(e){
                                this.setState({error:e.response.data.message})
                            }
                        }
                        else{
                            this.setState({error:"All fields are mandatory!"})
                        }
                    }}>Register</button>
                    <button onClick={(e)=>{this.props.history.push('/login')}} >Cancel</button>
                </div>
                
                {eroare}
            </div>
        );
        
    }
    
}

export default withRouter(Register);