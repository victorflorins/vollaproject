import React from 'react';
import './Welcome.css'

class Welcome extends React.Component {
    
    render(){
        return(
            <div className="welcome">
                <img alt="welcome" src="welcome-back.png" draggable="false"/>
            </div>
        );
    }
    
}

export default Welcome