import React from 'react';
import { Switch, Route, withRouter, Redirect } from "react-router-dom";
import Menu from '../Menu/Menu';
import MyProducts from '../MyProducts/MyProducts';
import Friends from '../Friends/Friends';
import Welcome from '../Welcome/Welcome';
import FriendsList from '../FriendsList/FriendsList';

class Home extends React.Component {
    
    
    render(){
        return (
            this.props.logat?
            <div>
                <Menu logout={this.props.logout}/>
                <Switch>
                    <Route exact path={'/'}>
                      <Welcome/>
                    </Route>
                    <Route exact path={'/myproducts'}>
                      <MyProducts userId={this.props.userId}/>
                    </Route>
                    <Route exact path={'/friends'}>
                      <Friends userId={this.props.userId}/>
                    </Route>
                    <Route exact path={'/friends/:userId'}>
                      <FriendsList currentUser={this.props.userId}/>
                    </Route>
                </Switch>
            </div>
            :
            <Redirect to="/login"/>
        );
    }
    
}

export default withRouter(Home)