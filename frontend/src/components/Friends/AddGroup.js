import React from 'react';
import axios from 'axios';
import {Dialog} from 'primereact/dialog';
import {Button} from 'primereact/button';
import {InputText} from 'primereact/inputtext';
import SERVER from '../../config/config';

class AddGroup extends React.Component {
    
    constructor(props){
        super(props)
        this.state={
            groupName:"",
            error:""
        }
    }
    
    render() {
        const footer = (
            <div>
                <Button label="Create" icon="pi pi-check" onClick={async ()=>{
                    this.setState({error:""})
                    if(this.state.groupName){
                        try{
                            let response=await axios.post(`${SERVER}/groups`,{
                                ownerId: this.props.userId,
                                label: this.state.groupName
                            })
                            if(response){
                                this.setState({error:"", groupName:""});
                                this.props.onHide(true);
                            }
                        }
                        catch(e){
                            this.setState({error:"Something went wrong!"})
                        }
                    }
                    else{
                        this.setState({error:"All fields are mandatory!"})
                    }
                    
                }} />
                <Button label="Cancel" icon="pi pi-times" onClick={()=>{this.setState({error:"", groupName:""});this.props.onHide(false)}} className="p-button-secondary" />
            </div>
        );

        return (
            <Dialog className="addGroupDialog" header="Create group" closable={false} closeOnEscape={false} focusOnShow={false} visible={this.props.visible} style={{width: '50vw'}} footer={footer} onHide={()=>{this.setState({error:"", groupName:""});this.props.onHide(false)}} modal={true}>
                <InputText type="text" value={this.state.groupName} onChange={(e)=>{this.setState({groupName:e.target.value})}} placeholder="Group name"/>
                {this.state.error && <div className="error" style={{color:"red"}}>{this.state.error}</div>}
            </Dialog>
        )
    
    }
}

export default AddGroup;