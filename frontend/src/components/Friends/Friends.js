import React from 'react';
import axios from 'axios';
import SERVER from '../../config/config';
import AddGroup from './AddGroup.js'
import Group from './Group.js'
import './Friends.css'

class Friends extends React.Component {
    
    constructor(props){
        super(props)
        this.state={
            groups:[],
            invitesSent: [],
            invitesReceived: [],
            error:"",
            addGroupDialogVisible:false
        }
        
        this.getInvitesSent=async () =>{
            try{
                let invitesSent=await axios.get(`${SERVER}/users/${this.props.userId}/invites_sent`);
                this._isMounted && this.setState({
                    invitesSent:invitesSent.data
                });
            }
            catch(e){
                this._isMounted && this.setState({error:"Something went wrong"})
            }
        }
        
        this.getInvitesReceived=async () =>{
            try{
                let invitesReceived=await axios.get(`${SERVER}/users/${this.props.userId}/invites_received`);
                this._isMounted && this.setState({
                    invitesReceived:invitesReceived.data
                });
            }
            catch(e){
                this._isMounted && this.setState({error:"Something went wrong"})
            }
        }
        
        this.getGroups=async () =>{
            try{
                let groups=await axios.get(`${SERVER}/users/${this.props.userId}/groups`);
                this._isMounted && this.setState({
                    groups:groups.data
                });
            }
            catch(e){
                this._isMounted && this.setState({error:e.response.data.message})
            }
        }
        
        this.sendInvite= async (id) =>{
            try{
                await axios.post(`${SERVER}/users/${this.props.userId}/invite`,{
                    invitedId: id
                })
                this._isMounted && this.getInvitesSent()
            }
            catch(e){
                this._isMounted && this.setState({error:e.response.data.message})
            }
        }
        
        this.uninvite= async (id) =>{
            try{
                await axios.post(`${SERVER}/users/${this.props.userId}/uninvite/${id}`)
                this._isMounted && this.getInvitesSent()
            }
            catch(e){
                this._isMounted && this.setState({error:e.response.data.message})
            }
        }
        
        this._isMounted = false;
    }
    
    componentDidMount = ()=>{
        this._isMounted = true;
        this._isMounted && this.getGroups()
        this._isMounted && this.getInvitesSent()
        this._isMounted && this.getInvitesReceived()
    }
    
    componentWillUnmount() {
       this._isMounted = false;
    }
    
    render(){
        return(
            <div className="Groups">
                <div className="header">
                    <h1>Groups of friends</h1>
                    <button className="createGroup" onClick={(e)=>{
                        this.setState({addGroupDialogVisible:true})
                    }}>Create group</button> 
                </div>
                
                {this.state.error && <div>{this.state.error}</div>}

                <AddGroup userId={this.props.userId} visible={this.state.addGroupDialogVisible} onHide={(refresh)=>{
                    this.setState({addGroupDialogVisible:false})
                    if(refresh){
                        this._isMounted && this.getGroups()
                    }
                }}/>
                
                {this.state.groups.map((group,index)=><Group key={index} userId={this.props.userId} group={group} sendInvite={this.sendInvite} invitesSent={this.state.invitesSent} invitesReceived={this.state.invitesReceived}
                                                            uninvite={this.uninvite} refreshList={()=>{this._isMounted && this.getGroups()}}/>)}
            </div>
        );
    }
    
}

export default Friends;