import React from 'react';
import axios from 'axios';
import SERVER from '../../config/config';
import Friend from './Friend';
import AddFriend from './AddFriend';
import './Group.css'

class Group extends React.Component {
    
    constructor(props){
        super(props)
        this.state={
            error:"",
            inviteFriendDialogVisible:false
        }
        
    }
    
    render(){
        
        let Buttons;
        if(this.props.group.ownerId===this.props.userId){
            Buttons=<div className="buttons">
                        <button  className="addFriend" onClick={(e)=>{this.setState({inviteFriendDialogVisible:true})}}>
                            <i className="pi pi-plus"></i>
                        </button>
                        <button  className="removeGroup" onClick={async (e)=>{
                            try{
                                let response=await axios.delete(`${SERVER}/groups/${this.props.group.id}`);
                                if(response){
                                    this.props.refreshList()
                                }
                            }
                            catch(e){
                                this.setState({error:e.response.data.message})
                            }
                        }}>
                            <i className="pi pi-trash"></i>
                        </button>
                     </div>
        }
        else{
            Buttons=<div className="buttons">
                        <button className="leaveGroup" onClick={ async (e)=>{
                            try{
                                let response=await axios.post(`${SERVER}/friends/delete`,{
                                    groupId:this.props.group.id,
                                    userId:this.props.userId
                                });
                                if(response){
                                    this.props.refreshList()
                                }
                            }
                            catch(e){
                                this.setState({error:e.response.data.message})
                            }
                        } }>Leave</button>
                    </div>
        }
        
        return (
            <div className="Group">
                <div className="header">
                    <span className="title">{this.props.group.label}</span>
                    { 
                        Buttons
                    }
                </div>
                {this.state.error && <div>{this.state.error}</div>}
                
                {this.props.group.owner && <Friend owner={true} friend={this.props.group.owner} sendInvite={this.props.sendInvite} invitesSent={this.props.invitesSent} 
                                                                    invitesReceived={this.props.invitesReceived} uninvite={this.props.uninvite}/>}
                
                {this.props.group.friends.filter(friend=>friend.user.id!==this.props.userId).map((friend,index)=><Friend key={index} friend={friend.user} sendInvite={this.props.sendInvite} invitesSent={this.props.invitesSent} 
                                                                    invitesReceived={this.props.invitesReceived} uninvite={this.props.uninvite}/>)}
                
                <AddFriend label={this.props.group.label} groupId={this.props.group.id} 
                        visible={this.state.inviteFriendDialogVisible} onHide={(refresh)=>{this.setState({inviteFriendDialogVisible:false}); if(refresh){this.props.refreshList()}}}/>
            </div>
        );
    }
    
}

export default Group