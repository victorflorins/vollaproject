import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import './Friend.css'

class Friend extends Component {
    
    
    render() {
        return (
          <div className="Friend">
            <div>{`Name: ${this.props.friend.firstName} ${this.props.friend.lastName}`+(this.props.owner?" (Owner)":"")}</div>
            
            <div className="friendActions">
              {
                this.props.invitesReceived.filter(item=>item.ownerId===this.props.friend.id).length>0 && <button className="seeList" onClick={(e)=>{
                  this.props.history.push({pathname:`/friends/${this.props.friend.id}`, state:{user: this.props.friend}})
                }}>See list</button>
              }
              
              {
                this.props.invitesSent.filter(item=>item.invitedId===this.props.friend.id).length>0?<button className="inviteButton" onClick={(e)=>{this.props.uninvite(this.props.friend.id)}}>Uninvite</button>:<button className="inviteButton" onClick={(e)=>{
                  this.props.sendInvite(this.props.friend.id)
                }}>Invite to watch my list</button>
              }
            </div>
          </div>
        );
    }
}

export default withRouter(Friend);