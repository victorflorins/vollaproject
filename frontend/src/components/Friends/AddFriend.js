import React from 'react';
import axios from 'axios';
import {Dialog} from 'primereact/dialog';
import {Button} from 'primereact/button';
import {InputText} from 'primereact/inputtext';
import SERVER from '../../config/config';

class AddFriend extends React.Component {
    
    constructor(props){
        super(props)
        this.state={
            email:"",
            error:""
        }
    }
    
    render() {
        const footer = (
            <div>
                <Button label="Add" icon="pi pi-check" onClick={async ()=>{
                    this.setState({error:""})
                    if(this.state.email){
                        try{
                            let response=await axios.post(`${SERVER}/friends`,{
                                groupId: this.props.groupId,
                                userEmail: this.state.email
                            })
                            if(response){
                                this.setState({error:"", email:""})
                                this.props.onHide(true);
                            }
                        }
                        catch(e){
                            this.setState({error:e.response.data.message})
                        }
                    }
                    else{
                        this.setState({error:"All fields are mandatory!"})
                    }
                }} />
                <Button label="Cancel" icon="pi pi-times" onClick={()=>{this.setState({error:"", email:""});this.props.onHide(false)}} className="p-button-secondary" />
            </div>
        );

        const header=`Add friend to ${this.props.label}`

        return (
            <Dialog className="addFriendDialog" header={header} closable={false} closeOnEscape={false} focusOnShow={false} visible={this.props.visible} style={{width: '50vw'}} footer={footer} onHide={()=>{this.setState({error:"", email:""});this.props.onHide(false)}} modal={true}>
                <InputText type="text" value={this.state.email} onChange={(e)=>{this.setState({email:e.target.value})}} placeholder="User email"/>
                {this.state.error && <div className="error" style={{color:"red"}}>{this.state.error}</div>}
            </Dialog>
        )
    
    }
}

export default AddFriend;