import React from 'react';
import axios from 'axios';
import {Dialog} from 'primereact/dialog';
import {Button} from 'primereact/button';
import {InputText} from 'primereact/inputtext';
import SERVER from '../../config/config';

class AddCategory extends React.Component {
    
    constructor(props){
        super(props)
        this.state={
            category:"",
            error:""
        }
    }
    
    render() {
        const footer = (
            <div>
                <Button label="Add" icon="pi pi-check" onClick={async ()=>{
                    this.setState({error:""})
                    if(this.state.category){
                        try{
                            let response=await axios.post(`${SERVER}/categories`,{
                                userId: this.props.userId,
                                name: this.state.category
                            })
                            if(response){
                                this.setState({error:"", category:""});
                                this.props.onHide(true);
                            }
                        }
                        catch(e){
                            this.setState({error:"Something went wrong!"})
                        }
                    }
                    else{
                        this.setState({error:"All fields are mandatory!"})
                    }
                    
                }} />
                <Button label="Cancel" icon="pi pi-times" onClick={()=>{this.setState({error:"", category:""});this.props.onHide(false)}} className="p-button-secondary" />
            </div>
        );

        return (
            <Dialog className="addCategoryDialog" header="Add category" closable={false} closeOnEscape={false} focusOnShow={false} visible={this.props.visible} style={{width: '50vw'}} footer={footer} onHide={()=>{this.setState({error:"", category:""});this.props.onHide(false)}} modal={true}>
                <InputText type="text" value={this.state.category} onChange={(e)=>{this.setState({category:e.target.value})}} placeholder="Category name"/>
                {this.state.error && <div className="error" style={{color:"red"}}>{this.state.error}</div>}
            </Dialog>
        )
    
    }
}

export default AddCategory;