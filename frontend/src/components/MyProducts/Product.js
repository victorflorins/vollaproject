import React, { Component } from 'react';
import axios from 'axios';
import SERVER from '../../config/config';
import './Product.css'

class Product extends Component {
    
    constructor(props){
        super(props);
        
        this.changeAvailability=async (e)=>{
            try{
                let response=await axios.put(`${SERVER}/products/${this.props.product.id}`,{
                    available: !this.props.product.available,
                    claimedBy: -1
                })
                if(response){
                    this.props.refreshList();
                }
            }
            catch(e){
            }
        }
    }
    
    
    
    render() {

        let expireDate=new Date(this.props.product.expireDate)
        let daysTillExpiry=(expireDate.getTime()-new Date().getTime())/ (1000 * 3600 * 24);
        
        
        return (
          <div className="Product">
            <div>
                <div>{this.props.product.name}</div>
                <div>Expires: {(expireDate.getDate()-1 < 10 ? '0' : '') + (expireDate.getDate()-1)+
                    "/"+(expireDate.getMonth()+1 < 10 ? '0' : '') + (expireDate.getMonth()+1)+
                    "/"+expireDate.getFullYear()}</div>
            </div>
            {
                daysTillExpiry<=1 && <div className="expiryWarning">
                    <i className="pi pi-exclamation-circle"></i>
                    <div>Expires soon</div>
                </div>
            }
            <div className="toRight">
                {
                    this.props.product.claimedBy!==-1 && <div>Claimed</div>
                }
                
                <button className="availableButton" onClick={this.changeAvailability}>{this.props.product.available?"Make unavailable":"Make available"}</button>
                
                <button className="removeProduct" onClick={async ()=>{
                    try{
                        let response = await axios.delete(`${SERVER}/products/${this.props.product.id}`)
                        if(response){
                            this.props.refreshList()
                        }
                    }
                    catch(e){
                    }
                }} style={{float:"right"}}><i className="pi pi-trash"></i></button>
            </div>
          </div>
        );
    }
}

export default Product;