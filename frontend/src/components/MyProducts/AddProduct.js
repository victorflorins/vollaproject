import React from 'react';
import axios from 'axios';
import {Dialog} from 'primereact/dialog';
import {Button} from 'primereact/button';
import {InputText} from 'primereact/inputtext';
import {Calendar} from 'primereact/calendar';
import SERVER from '../../config/config';

class AddProduct extends React.Component {
    
    constructor(props){
        super(props)
        this.state={
            name:"",
            expireDate:null,
            error:""
        }
    }
    
    render() {
        const footer = (
            <div>
                <Button label="Add" icon="pi pi-check" onClick={async ()=>{
                    this.setState({error:""})
                    if(this.state.name&&this.state.expireDate){
                        try{
                            let payload={
                                name: this.state.name,
                                expireDate:this.state.expireDate,
                                available:false,
                                userId: this.props.userId,
                                categoryId:this.props.categoryId
                            }
                            let response=await axios.post(`${SERVER}/products`,payload)
                            if(response){
                                this.setState({error:"", name:"", expireDate:null});
                                this.props.onHide(true);
                            }
                            
                        }
                        catch(e){
                            this.setState({error:"Something went wrong!"})
                        }
                    }
                    else{
                        this.setState({error:"All fields are mandatory!"})
                    }
                }} />
                <Button label="Cancel" icon="pi pi-times" onClick={()=>{this.setState({error:"", name:"", expireDate:null});this.props.onHide(false)}} className="p-button-secondary" />
            </div>
        );

        const header=`Add product to ${this.props.categoryName}`

        return (
            <Dialog className="addProductDialog" header={header} closable={false} closeOnEscape={false} focusOnShow={false} visible={this.props.visible} style={{width: '50vw'}} footer={footer} onHide={()=>{this.setState({error:"", name:"", expireDate:null});this.props.onHide(false)}} modal={true}>
                <InputText type="text" value={this.state.name} onChange={(e)=>{this.setState({name:e.target.value})}} placeholder="Name"/>
                <Calendar readOnlyInput={true} value={this.state.expireDate} onChange={(e) => this.setState({expireDate: e.value})} placeholder="Expire date"/>
                {this.state.error && <div className="error" style={{color:"red", margin: "0 10px" }}>{this.state.error}</div>}
            </Dialog>
        )
    
    }
}

export default AddProduct;