import React from 'react';
import axios from 'axios';
import SERVER from '../../config/config';
import Product from './Product';
import AddProduct from './AddProduct'
import "./Category.css"


class Category extends React.Component {
    
    constructor(props){
        super(props)
        this.state={
            products:[],
            error:"",
            addProductDialogVisible:false
        }
        
        this.getProducts=async () =>{
            try{
                let products=await axios.get(`${SERVER}/categories/${this.props.category.id}/products`);
                this._isMounted && this.setState({
                    products:products.data
                });
            }
            catch(e){
                this._isMounted && this.setState({error:e.response.data.message})
            }
        }
        
        this._isMounted = false;
    }
    
    componentDidMount = ()=>{
        this._isMounted = true;
        this._isMounted && this.getProducts()
    }
    
    componentWillUnmount() {
       this._isMounted = false;
    }
    
    render(){
        return (
            <div className="Category">
                <div className="header">
                    <span className="title">{this.props.category.name}</span>
                    <button className="addProduct" onClick={(e)=>{
                        this.setState({addProductDialogVisible:true})
                    }}><i className="pi pi-plus"></i></button>
                </div>
                {this.state.error && <div>{this.state.error}</div>}
                
                {this.state.products.map((product,index)=><Product key={index} product={product} refreshList={(e)=>{this.getProducts()}}/>)}
                
                <AddProduct categoryName={this.props.category.name} categoryId={this.props.category.id} userId={this.props.userId} 
                        visible={this.state.addProductDialogVisible} onHide={(refresh)=>{
                    this.setState({addProductDialogVisible:false})
                    if(refresh){
                        this._isMounted && this.getProducts()
                    }
                }}/>
            </div>
        );
    }
    
}

export default Category