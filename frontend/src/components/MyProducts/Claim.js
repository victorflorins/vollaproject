import React, { Component } from 'react';
import axios from 'axios';
import SERVER from '../../config/config';
import './Product.css'

class Claim extends Component {
    
    render() {

        let expireDate=new Date(this.props.product.expireDate)
        let daysTillExpiry=(expireDate.getTime()-new Date().getTime())/ (1000 * 3600 * 24);
        
        return (
          <div className="Product">
            <div>
                <div>{this.props.product.name}</div>
                <div>Expires: {(expireDate.getDate()-1 < 10 ? '0' : '') + (expireDate.getDate()-1)+
                    "/"+(expireDate.getMonth()+1 < 10 ? '0' : '') + (expireDate.getMonth()+1)+
                    "/"+expireDate.getFullYear()}</div>
            </div>
            {
                daysTillExpiry<=1 && <div className="expiryWarning">
                    <i className="pi pi-exclamation-circle"></i>
                    <div>Expires soon</div>
                </div>
            }
            
            <button className="claimButton" onClick={async (e)=>{
                                try{
                                    let res=await axios.post(`${SERVER}/products/${this.props.product.id}/unclaim`,{
                                        userId:this.props.currentUser
                                    })
                                    if(res){
                                        this.props.refreshList()
                                    }
                                }
                                catch(e){
                                }
                            }}>Unclaim</button>
            
          </div>
        );
    }
}

export default Claim;