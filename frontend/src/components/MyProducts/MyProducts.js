import React from 'react';
import axios from 'axios';
import Category from './Category';
import SERVER from '../../config/config';
import AddCategory from './AddCategory';
import './MyProducts.css';
import Claim from './Claim'


class MyProducts extends React.Component {
    
    constructor(props){
        super(props);
        this.state={
            categories: [],
            claims: [],
            error:"",
            addCategoryDialogVisible:false
        }
        
        this.getCategories=async () =>{
            try{
                let categories=await axios.get(`${SERVER}/users/${this.props.userId}/categories`);
                this._isMounted && this.setState({
                    categories:categories.data
                });
            }
            catch(e){
                this._isMounted && this.setState({error:"Something went wrong"})
            }
        }
        
        this.getClaims=async () =>{
            try{
                let claims=await axios.get(`${SERVER}/users/${this.props.userId}/claims`);
                this._isMounted && this.setState({
                    claims:claims.data
                });
            }
            catch(e){
                this._isMounted && this.setState({error:"Something went wrong"})
            }
        }
    
        this._isMounted = false;
    }
    
    componentDidMount = ()=>{
        this._isMounted = true;
        this._isMounted && this.getCategories()
        this._isMounted && this.getClaims()
    }
    
    componentWillUnmount() {
       this._isMounted = false;
    }
    
    render() {
        return (
            <div className="ProductList">
                <div className="header">
                    <h1>My Products</h1>
                    <button className="addCategory" onClick={(e)=>{
                        this.setState({addCategoryDialogVisible:true})
                    }}>Add category</button> 
                </div>
                {this.state.error && <div>{this.state.error}</div>}
                
                <AddCategory userId={this.props.userId} visible={this.state.addCategoryDialogVisible} onHide={(refresh)=>{
                    this.setState({addCategoryDialogVisible:false})
                    if(refresh){
                        this._isMounted && this.getCategories()
                    }
                }}/>
                
                {this.state.categories.map((category,index)=><Category key={index} userId={this.props.userId} category={category}/>)}
                
                <h1 style={{marginTop:"100px"}}>Claims</h1>
                {this.state.claims.map((claim,index)=><Claim key={index} product={claim} currentUser={this.props.userId} refreshList={()=>{
                    this._isMounted && this.getClaims()
                }}/>)}
            </div>
        );
    }
}

export default MyProducts;